<ol class="breadcrumb">
    <li><a href="<?php echo url('/'); ?>">หน้าแรก</a></li>
    <li class="active"><?php echo $title; ?></li>
</ol>

<div id="about_us" class="clearfix">
    <?php echo render($content['body']); ?>
</div>
