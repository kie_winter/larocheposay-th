<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<ol class="breadcrumb">
    <li><a href="<?php echo url('/'); ?>">หน้าแรก</a></li>
    <li class="active"><?php echo $view->get_title(); ?></li>
</ol>

<div id="news" class="clearfix">
    <h1><?php echo $view->get_title(); ?></h1>
    <div class="news-nav nav col-sm-3">
        <?php
        $vocabulary = taxonomy_vocabulary_machine_name_load('expert_talk');
        $terms = taxonomy_get_tree($vocabulary->vid);
        $taxonomy = array();
        foreach ($terms as $term) {
            if ($term->parents[0] == 0) {
                $taxonomy[$term->tid] = [
                    'tid' => $term->tid,
                    'name' => $term->name,
                    'link' => str_replace(' ', '-', strtolower($term->name)),
                    'child' => []
                ];
            } else {
                $taxonomy[$term->parents[0]]['child'][$term->tid] = [
                    'tid' => $term->tid,
                    'name' => $term->name,
                    'link' => str_replace(' ', '-', strtolower($term->name)),
                ];
            }
        }
        ?>
        <ul class="nav">
            <?php foreach ($taxonomy as $term): ?>
                <li><?php echo count($term['child']) <= 0 ? l($term['name'], 'expert-talk/' . $term['link'], ['absolute' => true]) : $term['name']; ?>
                    <ul class="inside" style="display: none;">
                        <?php foreach($term['child'] as $child): ?>
                            <li><?php echo l($child['name'], 'expert-talk/' . $child['link'], array('absolute' => true)); ?></li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div class="news-cont col-sm-9">
        <?php if ($rows): ?>
            <div class="view-content">
                <?php print $rows; ?>
            </div>
        <?php endif; ?>

        <nav>
            <?php if ($pager): ?>
                <?php print $pager; ?>
            <?php endif; ?>
        </nav>
    </div>
</div><?php /* class view */ ?>
