<?php
/**
 * Created by PhpStorm.
 * User: iitumii
 * Date: 5/30/2017 AD
 * Time: 12:20 PM
 */


/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>

<?php if ($rows) {?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">


    <?php foreach ($rows as $id => $row): ?>
        <?php print $row; ?>
    <?php endforeach; ?>
    </div>
</div>
<?php }?>
