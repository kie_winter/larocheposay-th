<?php print render($page['header']); ?>

<div class="container">
    <div class="header clearfix">
       <a href="<?php echo url('node/1', array('absolute' => true)); ?>">
           <h1>
               <div class="hidden-xs" >
                   <img alt="La Roche-Posay ลาโรช โพเซย์ " src="<?php echo base_path() . path_to_theme(); ?>/images/la_roche-logo_V2.png" class="pull-left img-responsive" >
               </div>

               <div class="visible-xs" style="display:block;margin:0auto;" >
                   <img alt="La Roche-Posay ลาโรช โพเซย์ " src="<?php echo base_path() . path_to_theme(); ?>/images/la_roche-logo_V2.png" style="display: block;margin: 0 auto;" >
                   <img  src="<?php echo base_path(). path_to_theme(); ?>/images/slogan01.png" alt="A BETTER LIFE FOR SENSITIVE SKIN" style="display: block;margin: 0 auto; margin-top: 5px;"   img-responsive">

               </div>


           </h1>
       </a>
       <h1>
           <img  src="<?php echo base_path(). path_to_theme(); ?>/images/slogan01.png" alt="A BETTER LIFE FOR SENSITIVE SKIN" class="slogan pull-left hidden-xs img-responsive">
       </h1>
        <div class="log-in pull-right  hidden-xs ">
            <h2><img src="<?php echo base_path(). path_to_theme(); ?>/images/slogan01.png" alt="A BETTER LIFE FOR SENSITIVE SKIN" class="slogan-mobile visible-xs img-responsive"></h2>
            <?php echo theme('links__menu_login', array('links' => menu_navigation_links('menu-login'))); ?>

            <div class="pull-righ ">
                <div class="fb-log">
                    <a href="https://www.facebook.com/LaRochePosayThailand"><h4><img src="<?php echo base_path() . path_to_theme(); ?>/images/fb-logo-sm.png" alt="https://www.facebook.com/LaRochePosayThailand"></h4></a>
                </div>
                <div class="search-box clearfix">
                    <gcse:search></gcse:search>
                </div>
            </div>
        </div>
    </div>

    <nav class="navbar navbar-defaul">
        <div class="navbar-header">
            <button type="button" class="navbar-left navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>


            <div class="search visible-xs" style="float: left; ">
                <gcse:search></gcse:search>
            </div>


            <div class="fb-log visible-xs" style="float: left; padding: 5px;">
                <a href="https://www.facebook.com/LaRochePosayThailand"><h4><img src="<?php echo base_path() . path_to_theme(); ?>/images/fb-logo-sm.png" alt="https://www.facebook.com/LaRochePosayThailand"></h4></a>
            </div>

            <div class="lazada-log visible-xs" style="float: left; padding: 5px;" >
                <a href="http://www.lazada.co.th/la-roche-posay-thailand/"><h4><img src="<?php echo base_path() . path_to_theme(); ?>/images/lazada.png" alt="http://www.lazada.co.th/la-roche-posay-thailand/"></h4></a>
            </div>
        </div>
        <div id="navbar" class="navbar-collapse collapse" aria-expanded="true" role="navigation">
            <?php if ($primary_nav): print $primary_nav; endif; ?>
        </div>
        <?php $products = larocheposay_taxonomy_menu('products'); ?>
        <div class="dropdown hidden-xs col-sm-offset-2" style="display: none;">
            <div id="first-sub" class="col-sm-4">
                <div id="me01">
                    <ul class="nav">
                        <?php foreach ($products as $product): ?>
                            <li class="first-product" data-id="<?php echo $product['tid']; ?>"><a href="<?php echo url('products/' . str_replace(' ', '-', $product['name']), array('absolute' => true)); ?>"><?php echo $product['name']; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>


    </nav>

    <?php echo render($page['content']); ?>

    <footer>
        <div class="footer-menu">
            <div class="row">
                <div class="col-sm-3">
                    <nav><?php echo theme('links__main_menu', array('links' => menu_navigation_links('main-menu'))); ?></nav>
                    
                </div>

                <div class="col-sm-3">
                    <p>สมาชิก La Roche-Posay</p>
                    <?php echo theme('links__menu_footer_menu', array('links' => menu_navigation_links('menu-footer-menu'))); ?>
                </div>

                <div class="social col-sm-2">
                    <p>SOCIAL NETWORK</p>
                    <ul>
                        <li><a href="https://www.facebook.com/LaRochePosayThailand" target="_blank"><h4><img src="<?php echo base_path() . path_to_theme(); ?>/images/fb-logo.png" alt="https://www.facebook.com/LaRochePosayThailand"></h4></a></li>
                        <li><a href="https://www.youtube.com/channel/UC0_SZhOGJL1pHteexU5eI5Q" target="_blank"><h4><img src="<?php echo base_path() . path_to_theme(); ?>/images/yt-logo.png" alt="youtube LaRochePosayThailand"></h4></a></li>
                    </ul>
                </div>
                <div class="col-sm-4">
	                <ul>
	                	<li><a href="https://www.larocheposay-th.com/term-of-use" target="_blank">เงื่อนไขการใช้</a></li>
	                	<li><a href="https://www.larocheposay-th.com/privacy-and-cookies-policy" target="_blank">นโยบายความเป็นส่วนตัว</a></li>
	                	<li><a href="https://www.larocheposay-th.com/terms-and-conditions" target="_blank">เงื่อนไขและข้อกำหนดการได้รับอนุญาตการโอลาปิคแบรนด์</a></li>
	                </ul>
                </div>    
            </div>
        </div>
    </footer>
</div>
<style type="text/css">
    /*.navbar-toggle.navbar-left {*/
        /*float: left;*/
        /*margin-left: 10px;*/
    /*}*/

    /*.icon-bar {*/
        /*background-color: white;*/
    /*}*/

    /*.search {*/
        /*width: 50%;*/
        /*height: 40px;*/
        /*overflow: hidden;*/
        /*background-color: #f8f6f4;*/
        /*..margin-top: 10px; }*/
    /*.search .input-search {*/
        /*display: inline-block; }*/
    /*.search .input-search .btn-search {*/
        /*border: 0;*/
        /*!*background-color: #f1edea;*!*/
        /*padding: 2px 10px; }*/
    /*.search .form-search {*/
        /*border: 0;*/
        /*border-radius: 0;*/
        /*background-color: #f8f6f4;*/
        /*font-size: 20px;*/
        /*padding: 0 5px 0 0;*/
        /*margin: 0;*/
        /*line-height: 30px;*/
        /*width: 100%;*/
        /*!*max-width: 200px;*!*/
        /*display: inline-block;*/
        /*border: white;*/
    /*}*/


    /*.search  .cse .gsc-search-button input.gsc-search-button-v2,*/
    /*.search  input.gsc-search-button-v2 {*/
        /*height: 26px !important;*/
        /*margin-top: 0 !important;*/
        /*min-width: 13px !important;*/
        /*padding: 5px 5px !important;*/
        /*width: 30px !important;*/
        /*border-color: #009de0 !important;*/
        /*background-color: #009de0 !important; }*/

    /*.search .gsc-search-box-tools .gsc-search-box .gsc-input,*/
    /*.search  table.gsc-search-box td,*/
    /*.gsc-input-box {*/
        /*background: #f8f6f4 !important;*/
        /*border: 0 !important;*/
        /*padding-top: 0 !important; }*/


    /*::-webkit-input-placeholder { !* Chrome *!*/
        /*color: transparent;*/
    /*}*/
    /*:-ms-input-placeholder { !* IE 10+ *!*/
        /*color: transparent;*/
    /*}*/
    /*::-moz-placeholder { !* Firefox 19+ *!*/
        /*color: transparent;*/
        /*opacity: 1;*/
    /*}*/
    /*:-moz-placeholder { !* Firefox 4 - 18 *!*/
        /*color: transparent;*/
        /*opacity: 1;*/
    /*}*/
</style>

<script type="text/javascript">


(function() {
    var cx = '000400970650938170948:2_es7kp5mbk';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
})();



</script>
