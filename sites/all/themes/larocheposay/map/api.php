
<?php

$url = 'https://docs.google.com/spreadsheets/d/1gjDCLmyvuUGcW3yyLlIxZg8L7MSt5erPxvki0i7hN78/pub?output=csv';
$arr = array();
$return = array();
$col = 0;
// open file for reading
if (($handle = fopen($url, "r")) !== FALSE)
{
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
    {
        $col++;
        if ($col != 1)
        {
        $totalrows = count($data);
        for ($row=0; $row<=$totalrows; $row++)
        {
                switch ($row) {
                    case 0:
                        $str = 'no';
                        $key = $row;
                        break;
                    case 1:
                        $str = 'name';
                        $key = $row;
                        break;
                    case 2:
                        $str = 'address';
                        $key = $row;
                        break;
                    case 3:
                        $str = 'tel';
                        $key = $row;
                        break;
                    case 4:
                        $str = 'position';
                        $key = $row;
                        break;
                    case 5:
                        $str = 'type';
                        $key = $row;
                        break;
                    default:
                        # code...
                        break;
                }
              
                $arr[$str] =$data[$key];
                
                $geo = explode(", ", $data[4]);
                $arr['lat'] = $geo[0]; 
                $arr['lng'] = $geo[1]; 
               
        }
        $return[] = $arr;
    }
    }
    fclose($handle);
}
echo json_encode($return);
?>
