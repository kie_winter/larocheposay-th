<!DOCTYPE html>
<html>
  <head>
    <title>Geolocation</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <meta charset="utf-8">
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 100%;
      }
      .plf-button{
	      cursor: pointer;
      }
    </style>
  </head>
  <body>

    <div id="map"></div>

    <script>
	   var pos;
	   var locations = [
	   		<?php
					$json = file_get_contents('https://larocheposay-th.com/sites/all/themes/larocheposay/map/api.php');
					$obj = json_decode($json,true);
					$length = count($data);
				
					if ($_GET['type']) {
						if($_REQUEST['type'] === "Store" || $_REQUEST['type'] === "Physician"){
							$type = $_REQUEST['type'];
						}else{
							$type = "all";
						}
					}else{
						$type = "all";
					}
				
					for($i=0; $i<count($obj); $i++) {
						
						if($obj[$i]['type'] === "Store" &&  $type === "Store"){
							echo "[\"".$obj[$i]['name']."\",\"".$obj[$i]['address']."\",\"".$obj[$i]['tel']."\",\"".$obj[$i]['type']."\", \"".$obj[$i]['lat']."\",\"".$obj[$i]['lng']."\"], ";
						
						}elseif($obj[$i]['type'] === "Physician" &&  $type === "Physician"){
							echo "[\"".$obj[$i]['name']."\",\"".$obj[$i]['address']."\",\"".$obj[$i]['tel']."\",\"".$obj[$i]['type']."\", \"".$obj[$i]['lat']."\",\"".$obj[$i]['lng']."\"], ";
						}elseif( $type === "all"){
							echo "[\"".$obj[$i]['name']."\",\"".$obj[$i]['address']."\",\"".$obj[$i]['tel']."\",\"".$obj[$i]['type']."\", \"".$obj[$i]['lat']."\",\"".$obj[$i]['lng']."\"], ";
						}
					}
				
			?>
		   
	   ];
	   
	   
	   var isMobile = {
	   		Android: function() {
		        return navigator.userAgent.match(/Android/i);
		    },
		    BlackBerry: function() {
		        return navigator.userAgent.match(/BlackBerry/i);
		    },
		    iOS: function() {
		        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		    },
		    Opera: function() {
		        return navigator.userAgent.match(/Opera Mini/i);
		    },
		    Windows: function() {
		        return navigator.userAgent.match(/IEMobile/i);
		    },
		    any: function() {
		        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		    }
		};
	    	 
	
      function initMap() {
	  
	   var directionsDisplay = new google.maps.DirectionsRenderer;
	   var directionsService = new google.maps.DirectionsService;
	   var iconShop = { url: 'https://larocheposay-th.com/sites/all/themes/larocheposay/map/img/icon-shop.png'};  
	   var iconMy = {url: 'https://larocheposay-th.com/sites/all/themes/larocheposay/map/img/icon-my3.png'};  
	   
	  
	       
		$( "body" ).delegate('.direction-serch',"click", function() {
			 
			var did = $(this).attr('data-id');
		 	var dlat = $(this).attr('data-lat-'+did);
		    var dlng = $(this).attr('data-lng-'+did);			 
			endD  = new google.maps.LatLng(dlat, dlng);

			
			if ( isMobile.Android() ) {
				window.open('google.navigation:q='+dlat+','+dlng+'&mode=d', '_system');
		    }
		    else if(isMobile.iOS()){
		        var url = "comgooglemaps://?q="+dlat+","+dlng+"&directionsmode=driving";
		        window.open(url);   
		    }else{
		      calculateAndDisplayRoute(directionsService, directionsDisplay);
		    }
	
		});  

		function calculateAndDisplayRoute(directionsService, directionsDisplay) {
		  directionsService.route({
		    origin: pos ,
		    destination: endD,
		    travelMode: google.maps.TravelMode.DRIVING
		  }, function(response, status) {
		    if (status === google.maps.DirectionsStatus.OK) {
		      directionsDisplay.setDirections(response);
		    } else {
		      window.alert('Directions request failed due to ' + status);
		    }
		  });
		}
	   
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 13.7249715, lng: 100.5038949},
          zoom: 13 
        });
        
        directionsDisplay.setMap(map);

        
        var infoWindow = new google.maps.InfoWindow({map: map});
		var marker, i;
	
        for (i = 0; i < locations.length; i++) {
          

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][4], locations[i][5]),
                map: map,
                icon: iconShop,
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {

                    LrpMapsListener = '<div  class="plf-shop"><div class="plf-name"><b>'+locations[i][0]+'</b></div><div class="plf-address">'+locations[i][1]+'</div><div class="plf-tel">'+locations[i][2]+'</div><div class="plf-type">'+locations[i][3]+'</div></div><div class="plf-button direction-serch" data-lat-'+i+'="'+locations[i][4]+'" data-lng-'+i+'="'+locations[i][5]+'" data-id="'+i+'"><div class="plf-button-image"> <img src="http://stage.larocheposay-th.com/sites/all/themes/larocheposay/map/img/search-by-map-direction.png" height="32" width="31" alt="" style="width: auto;"> </div></div>';
					infoWindow.setContent(LrpMapsListener);
                    infoWindow.open(map, marker);
                }
            
            })(marker, i));
        }
        

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

			marker = new google.maps.Marker({
                position: pos,
                map: map,
                icon: iconMy
            });

		  map.setCenter(pos);

          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }
      }

      
    </script>
     <div class="direction-serch" ></div>
 <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDAl4puKCMPE0ZZ0bTPl37zSb-L7b5izIU&callback=initMap">
    </script>
  </body>
</html>