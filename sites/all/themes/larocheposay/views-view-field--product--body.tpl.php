<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>
<a href="<?php echo url('node/' . $row->_field_data['nid']['entity']->nid, array('absolute' => true)); ?>">
  <img src="<?php echo file_create_url($row->_field_data['nid']['entity']->field_small_thumbnail['und'][0]['uri']); ?>" title="<?php echo $row->_field_data['nid']['entity']->field_small_thumbnail['und'][0]['title']; ?>" alt="<?php echo $row->_field_data['nid']['entity']->field_small_thumbnail['und'][0]['alt']; ?>" class="img-responsive">
  <p class="product-name"><?php echo $row->_field_data['nid']['entity']->title; ?></p>
  <p><?php print $output; ?></p>
</a>
