<div id="products" class="inside clearfix">
    <ol class="breadcrumb">
        <li><?php echo l('หน้าแรก', '', array('absolute' => true)); ?></li>
        <?php
        $parent = taxonomy_get_parents($node->field_category['und'][0]['tid']);
        $parent = array_shift($parent);
        ?>
        <?php if ($parent): ?><li><?php echo l($parent->name, 'products/' . str_replace(' ', '-', $parent->name), array('absolute' => true)); ?></li><?php endif; ?>
        <?php if (isset($node->field_category['und'][0]['taxonomy_term'])): ?>
            <li><?php echo l($node->field_category['und'][0]['taxonomy_term']->name, 'products/' . str_replace(' ', '-', $node->field_category['und'][0]['taxonomy_term']->name), array('absolute' => true)); ?></li>
            <li class="active"><?php echo $node->title; ?></li>
        <?php else: ?>
            <li class="active"><?php echo $node->field_category['und'][0]['taxonomy_term']->name; ?></li>
        <?php endif; ?>
    </ol>

    <div class="product-item">
        <h2><?php echo $node->field_category['und'][0]['taxonomy_term']->name; ?></h2>

        <div class="product-pic col-md-4">
            <img src="<?php echo file_create_url($node->field_thumbnail['und'][0]['uri']); ?>" alt="<?php echo $node->field_thumbnail['und'][0]['alt']; ?>" title="<?php echo $node->field_thumbnail['und'][0]['title']; ?>" class="img-responsive">
        </div>
        <div class="col-md-8">
            <div class="product-detail">
                <h1><?php echo $node->title; ?></h1>
                <p><?php echo render($content['body']); ?></p>
                
                <?php  if (isset($node->field_ingredient['und'])): ?>
                <dl style="clear: both;">
                    <dt>ส่วนประกอบ</dt>
                    <dd class="Ingredient" ><?php echo $node->field_ingredient['und'][0]['value']; ?></dd>
                </dl>
				<?php  endif; ?>
				
				
				
                <dl style="clear: both;">
                    <dt>วิธีใช้</dt>
                    <dd><?php if (isset($node->field_how_to_use['und'])): echo $node->field_how_to_use['und'][0]['value']; endif; ?></dd>
                </dl>

                <dl>
                    <dt>ขนาด :</dt>
                    <dd><?php if (isset($node->field_size['und'])): echo $node->field_size['und'][0]['value']; endif; ?></dd>
                </dl>

                <dl>
                    <dt>ราคา :</dt>
                    <dd><?php if (isset($node->field_cost['und'])): echo number_format($node->field_cost['und'][0]['value']); endif; ?> บาท</dd>
                </dl>

                <?php if (isset($node->field_shopping_link['und'])): ?>
                    <a class="buy-now"  onclick="gaClickTracking('<?php echo $node->title; ?>');" href="<?php echo $node->field_shopping_link['und'][0]['value']; ?>"  target="_blank">
                        <img src="<?php echo base_path() . path_to_theme(); ?>/images/shopping_button.png" border="0">
                    </a>
                <?php endif; ?>
                <?php  if (isset($node->field_product_info['und'])): ?>
                <dl>
                    <dt></dt>
                    <dd><?php echo $node->field_product_info['und'][0]['value']; ?></dd>
                </dl>
				<?php  endif; ?>
            </div>
        </div>
    </div>
</div>
