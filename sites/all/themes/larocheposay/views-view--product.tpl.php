<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<div id="products" class="clearfix">
    <div class="top-banner">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <?php $arg = arg(); ?>
                <?php echo views_embed_view('product_main_banner', 'default', $arg[1]); ?>
            </div>
        </div>
    </div>
    <ol class="breadcrumb">
        <li><?php echo l('หน้าแรก', '', array('absolute' => true)); ?></li>
        <?php if ($view->args[0] == 'all'): ?>
            <li class="active">ทั้งหมด</li>
        <?php else: ?>
            <?php
            $term = taxonomy_term_load($view->argument['term_node_tid_depth']->argument);
            $parent = taxonomy_get_parents($term->tid);
            $parent = array_shift($parent);
            ?>
            <?php if ($parent): ?><li><?php echo l($parent->name, 'products/' . str_replace(' ', '-', $parent->name), array('absolute' => true)); ?></li><?php endif; ?>
            <li class="active"><?php echo $term->name; ?></li>
        <?php endif; ?>
    </ol>
    <div class="product-item">
        <h2><?php echo $view->args[0] == 'all' ? 'ทั้งหมด' : $term->name; ?></h2>
        <?php $products = larocheposay_taxonomy_menu('products'); ?>
        <div class="sidenav col-md-3">
            <ul class="nav">
                <?php foreach ($products as $product): ?>
                    <li class="parent <?php echo $parent && ($parent->tid == $product['tid']) || !$product && ($term->tid == $product['tid'])  ? 'active' : ''; ?>"><?php echo count($product['child']) > 0 ? $product['name'] : l($product['name'], 'products/' . str_replace(' ', '-', $product['name']), array('absolute' => true)); ?>
                        <?php if (count($product['child']) > 0): ?>
                            <ul class="nav sub-nav" <?php echo $parent->tid == $product['tid'] ? '' : 'style="display: none;"'; ?>>
                                <?php foreach ($product['child'] as $child): ?>
                                    <li><?php echo l($child['name'], 'products/' . str_replace(' ', '-', $child['name']), array('absolute' => true)); ?></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="product-list col-md-9">
            <?php if ($rows): print $rows; endif; ?>
        </div>
    </div>
</div>
