<?php
/**
 * Created by PhpStorm.
 * User: iitumii
 * Date: 5/30/2017 AD
 * Time: 12:21 PM
 */


/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>
<div class="col-xs-4 col-sm-3 col-md-2 nopadding">
    <div class="instagram-cell">
        <a href="#modal-instagram-<?php echo ($row->nid); ?>" data-toggle="modal" >
            <img class="lazy img-responsive"  width="153" height="153" data-original="<?php echo file_create_url($row->_field_data['nid']['entity']->field_images_ig_thumbnail['und'][0]['uri']); ?>"  alt="<?php echo $row->_field_data['nid']['entity']->title; ?>"  />
        </a>
    </div>
</div>
<div class="modal-instagram portfolio-modal modal fade " id="modal-instagram-<?php echo ($row->nid); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="lb-instagram col-md-12 col-sm-12">
                <div class="col-md-6 col-sm-5 padding-none" >
                <img  class="img-responsive" src="<?php echo file_create_url($row->_field_data['nid']['entity']->field_images_ig['und'][0]['uri']); ?>"/></div>
                    <div class="col-md-6 col-sm-7 padding-none">
                    <h2 class="name-ig"><?php echo ($row->_field_data['nid']['entity']->field_name_ig['und'][0]['value']); ?></h2>
                    <div class="col-md-12 col-sm-12">
                        <div class="col-md-5 col-sm-5 col-xs-12 ">
                            <img   class="img-responsive" src="<?php echo file_create_url($row->_field_data['nid']['entity']->field_product_images_ig['und'][0]['uri']); ?>"/></div>
                        <div class="col-md-7 col-sm-7 col-xs-12 ">
                            <h3 class="product-name-ig"><?php echo ($row->_field_data['nid']['entity']->field_product_name_ig['und'][0]['value']); ?></h3>
                            <div class="product-info-ig">
                                <?php echo ($row->_field_data['nid']['entity']->field_product_info_ig['und'][0]['value']); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
	                    <div class="block-button">
	                        <div class="button btn-shopnow">
	                            <a class="buy-now" onclick="gaClickTrackingClick('instagram','click','shop-now');" href="<?php echo ($row->_field_data['nid']['entity']->field_shop_now_ig['und'][0]['value']); ?>">
	                                <img src="<?php echo base_path() . path_to_theme(); ?>/images/icon_lazada.png" class="img-responsive">
	                                Shop now
	                            </a>
	                        </div>
	                        <div class="button btn-readmore">
	                            <a onclick="gaClickTrackingClick('instagram','click','readmore');" href="<?php echo ($row->_field_data['nid']['entity']->field_read_more_ig['und'][0]['value']); ?>">
	                                <img src="<?php echo base_path() . path_to_theme(); ?>/images/icon_readmore.png" class="img-responsive">
	                                Read more
	                            </a>
	                        </div>
	                    </div>    
                    </div>
                </div>
                <button type="button" class="close-modal" data-dismiss="modal">close</button>
            </div>
        </div>
    </div>
</div>


