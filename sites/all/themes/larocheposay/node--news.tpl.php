<ol class="breadcrumb">
    <li><a href="<?php echo url('/'); ?>">หน้าแรก</a></li>
    <li><a href="<?php echo url('news/all'); ?>">ข่าวและโปรโมชั่น</a></li>
    <li class="active"><?php echo $node->title; ?></li>
</ol>

<div id="detail_page" class="clearfix">
    <h1><?php echo $node->title; ?></h1>
    <div class="page-box clearfix">
        <?php if (isset($node->field_image['und'])): ?>
            <div class="pic col-md-5">
                <img src="<?php echo file_create_url($node->field_image['und'][0]['uri']); ?>" alt="<?php echo $node->field_image['und'][0]['alt']; ?>" title="<?php echo $node->field_image['und'][0]['title']; ?>" class="img-responsive">
            </div>
        <?php endif; ?>
        <div class="des <?php echo isset($node->field_image['und']) ? 'col-md-7' : 'col-md-12'; ?>">
            <?php echo render($content['body']); ?>
        </div>
    </div>
</div>
