<ol class="breadcrumb">
    <li><a href="<?php echo url('/'); ?>">หน้าแรก</a></li>
    <li class="active"><?php echo $title; ?></li>
</ol>
<div id="contact_us" class="clearfix">
    <h1><?php echo $title; ?></h1>
    <?php echo render($content['body']); ?>
    <div class="loc clearfix">
        <h2>Location</h2>
        <p>ค้นหา ลาโรช-โพเซย์ ทั่วประเทศ</p>
        <ul id="loc-nav" class="nav navbar-nav">
            <?php
            $vocabulary = taxonomy_vocabulary_machine_name_load('location');
            $terms = taxonomy_get_tree($vocabulary->vid);
            ?>
            <?php foreach ($terms as $term): ?>
                <li id="loc-<?php echo $term->tid; ?>" <?php echo $term->tid == '38' ? 'class="active"' : ''; ?>><?php echo $term->name; ?></li>
            <?php endforeach; ?>
        </ul>
        <div id="for-bkk" class="inner">
            <ul class="place nav">
                <li><h3>โรงพยาบาล</h3>
                    <?php echo views_embed_view('location', 'default', 'กรุงเทพฯ', 'โรงพยาบาล'); ?>
                </li>
                <li><h3>คลีนิค</h3>
                    <?php echo views_embed_view('location', 'default', 'กรุงเทพฯ', 'คลีนิค'); ?>
                </li>
                <li><h3><img src="<?php echo base_path() . path_to_theme(); ?>/images/contact-boots.png" class="img-responsive"></h3>
                    <?php echo views_embed_view('location', 'default', 'กรุงเทพฯ', 'Boots'); ?>
                </li>
                <li><h3><img src="<?php echo base_path() . path_to_theme(); ?>/images/contact-watsons.png" class="img-responsive"></h3>
                    <?php echo views_embed_view('location', 'default', 'กรุงเทพฯ', 'Watson'); ?>
                </li>
            </ul>
        </div>
        <div id="for-central" class="inner" style="display: none;">
            <ul class="place nav">
                <li><h3>โรงพยาบาล</h3>
                    <?php echo views_embed_view('location', 'default', 'ภาคกลาง', 'โรงพยาบาล'); ?>
                </li>
                <li><h3>คลีนิค</h3>
                    <?php echo views_embed_view('location', 'default', 'ภาคกลาง', 'คลีนิค'); ?>
                </li>
                <li><h3><img src="<?php echo base_path() . path_to_theme(); ?>/images/contact-boots.png" class="img-responsive"></h3>
                    <?php echo views_embed_view('location', 'default', 'ภาคกลาง', 'Boots'); ?>
                </li>
                <li><h3><img src="<?php echo base_path() . path_to_theme(); ?>/images/contact-watsons.png" class="img-responsive"></h3>
                    <?php echo views_embed_view('location', 'default', 'ภาคกลาง', 'Watson'); ?>
                </li>
            </ul>
        </div>
        <div id="for-east" class="inner" style="display: none;">
            <ul class="place nav">
                <li><h3>โรงพยาบาล</h3>
                    <?php echo views_embed_view('location', 'default', 'ภาคตะวันออก', 'โรงพยาบาล'); ?>
                </li>
                <li><h3>คลีนิค</h3>
                    <?php echo views_embed_view('location', 'default', 'ภาคตะวันออก', 'คลีนิค'); ?>
                </li>
                <li><h3><img src="<?php echo base_path() . path_to_theme(); ?>/images/contact-boots.png" class="img-responsive"></h3>
                    <?php echo views_embed_view('location', 'default', 'ภาคตะวันออก', 'Boots'); ?>
                </li>
                <li><h3><img src="<?php echo base_path() . path_to_theme(); ?>/images/contact-watsons.png" class="img-responsive"></h3>
                    <?php echo views_embed_view('location', 'default', 'ภาคตะวันออก', 'Watson'); ?>
                </li>
            </ul>
        </div>
        <div id="for-north-east" class="inner" style="display: none;">
            <ul class="place nav">
                <li><h3>โรงพยาบาล</h3>
                    <?php echo views_embed_view('location', 'default', 'ภาคตะวันออกเฉียงเหนือ', 'โรงพยาบาล'); ?>
                </li>
                <li><h3>คลีนิค</h3>
                    <?php echo views_embed_view('location', 'default', 'ภาคตะวันออกเฉียงเหนือ', 'คลีนิค'); ?>
                </li>
                <li><h3><img src="<?php echo base_path() . path_to_theme(); ?>/images/contact-boots.png" class="img-responsive"></h3>
                    <?php echo views_embed_view('location', 'default', 'ภาคตะวันออกเฉียงเหนือ', 'Boots'); ?>
                </li>
                <li><h3><img src="<?php echo base_path() . path_to_theme(); ?>/images/contact-watsons.png" class="img-responsive"></h3>
                    <?php echo views_embed_view('location', 'default', 'ภาคตะวันออกเฉียงเหนือ', 'Watson'); ?>
                </li>
            </ul>
        </div>
        <div id="for-south" class="inner" style="display: none;">
            <ul class="place nav">
                <li><h3>โรงพยาบาล</h3>
                    <?php echo views_embed_view('location', 'default', 'ภาคใต้', 'โรงพยาบาล'); ?>
                </li>
                <li><h3>คลีนิค</h3>
                    <?php echo views_embed_view('location', 'default', 'ภาคใต้', 'คลีนิค'); ?>
                </li>
                <li><h3><img src="<?php echo base_path() . path_to_theme(); ?>/images/contact-boots.png" class="img-responsive"></h3>
                    <?php echo views_embed_view('location', 'default', 'ภาคใต้', 'Boots'); ?>
                </li>
                <li><h3><img src="<?php echo base_path() . path_to_theme(); ?>/images/contact-watsons.png" class="img-responsive"></h3>
                    <?php echo views_embed_view('location', 'default', 'ภาคใต้', 'Watson'); ?>
                </li>
            </ul>
        </div>
        <div id="for-north" class="inner" style="display: none;">
            <ul class="place nav">
                <li><h3>โรงพยาบาล</h3>
                    <?php echo views_embed_view('location', 'default', 'ภาคเหนือ', 'โรงพยาบาล'); ?>
                </li>
                <li><h3>คลีนิค</h3>
                    <?php echo views_embed_view('location', 'default', 'ภาคเหนือ', 'คลีนิค'); ?>
                </li>
                <li><h3><img src="<?php echo base_path() . path_to_theme(); ?>/images/contact-boots.png" class="img-responsive"></h3>
                    <?php echo views_embed_view('location', 'default', 'ภาคเหนือ', 'Boots'); ?>
                </li>
                <li><h3><img src="<?php echo base_path() . path_to_theme(); ?>/images/contact-watsons.png" class="img-responsive"></h3>
                    <?php echo views_embed_view('location', 'default', 'ภาคเหนือ', 'Watson'); ?>
                </li>
            </ul>
        </div>
         </div>
        
	  <h2>ค้นหา ลาโรช-โพเซย์ ทั่วประเทศ ผ่านแผนที่ </h2>
      <p>กรุณาเรื่องประเภทร้านค้าที่ท่านต้องการ</p>
    <div class="locM clearfix"> 
		<ul id="locM-nav" class="nav navbar-nav">
		    <li id="locM-01" data-url="https://www.larocheposay-th.com/sites/all/themes/larocheposay/map/"  onclick="geomapiF(this);" class="active">Locate All</li>
		    <li id="locM-02" data-url="https://www.larocheposay-th.com/sites/all/themes/larocheposay/map/?type=Store"  onclick="geomapiF(this);" >Locate A Store</li>
		    <li id="locM-03" data-url="https://www.larocheposay-th.com/sites/all/themes/larocheposay/map/?type=Physician"  onclick="geomapiF(this);" >Locate A Hospital</li>
		</ul>
    </div> 
    
   
	<div id="geomap" >
    	<iframe id="myIFrame" width="100%" height="350px" src="https://www.larocheposay-th.com/sites/all/themes/larocheposay/map/"></iframe>
	</div>
  
    <div>
        
        
    </div>
</div>

<script type="text/javascript">
	function geomapiF(url){
		$("#myIFrame").attr('src', url.getAttribute("data-url"));
	}
</script>
