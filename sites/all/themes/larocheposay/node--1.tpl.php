<div id="home">
  <?php
    $nids = [];
    $results = db_query("SELECT node.nid AS nid, nodequeue_nodes_node.position AS nodequeue_nodes_node_position FROM {node} node INNER JOIN {nodequeue_nodes} nodequeue_nodes_node ON node.nid = nodequeue_nodes_node.nid AND nodequeue_nodes_node.qid = '1' WHERE (( (node.status = 1 OR (node.uid = 1 AND 1 <> 0 AND 1 = 1) OR 1 = 1) AND (node.type IN  ('highlight')) )) ORDER BY nodequeue_nodes_node_position ASC");
    foreach ($results as $row) {
      $nids[] = $row->nid;
    }
  ?>
  <?php if (count($nids) > 0): ?>
    <div class="top-banner">

      <div id="myCarousel" class="carousel slide" data-ride="carousel">

        <ol class="carousel-indicators">
          <?php for ($i = 0; $i < count($nids); $i++): ?>
            <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" <?php echo $i == 0 ? 'class="active"' : ''; ?>><h5></h5></li>
          <?php endfor; ?>
        </ol>

        <div class="carousel-inner" role="listbox">
          <?php echo views_embed_view('main_banner', 'default'); ?>
        </div>

      </div>
      <div class="block-instagram">
        <img   src="<?php echo base_path() . path_to_theme(); ?>/images/icon-freefromacne.png" class="img-responsive" style="display: block;margin: 0 auto;padding: 20px;max-width: 240px;"/>
			<?php echo views_embed_view('instagram', 'default'); ?>
       </div>

    </div>
  <?php endif; ?>

  <div class="home-content clearfix">
    <?php echo views_embed_view('body_banner', 'default'); ?>
    <?php echo views_embed_view('bottom_banner', 'default'); ?>
  </div>
</div>
<style>
    .nopadding {
        padding: 2px !important;
        margin: 1 !important;
    }

</style>