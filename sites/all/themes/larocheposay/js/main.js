!function(c){Drupal.behaviors.myBehavior={attach:function(o,t){c(document).ready(function(){c("img.lazy").show().lazyload(),c(".menu-340").click(function(){c(".dropdown").slideToggle("slow")}),c("#products .sidenav > .nav > li").click(function(){c(this).toggleClass("active"),c(this).children(".sub-nav").slideToggle("slow")}),c("#products .sidenav > .nav > .active .sub-nav").removeAttr("style"),c("#news .news-nav>.nav>li").click(function(){c(this).toggleClass("active"),c(this).children(".inside").slideToggle("fast")}),c("#contact_us .loc #loc-nav li").click(function(){c("#contact_us .loc #loc-nav li").removeClass("active"),c(this).toggleClass("active"),c("#contact_us .loc .inner").hide()}),c("#contact_us .locM #locM-nav li").click(function(){c("#contact_us .locM #locM-nav li").removeClass("active"),c(this).toggleClass("active"),c("#contact_us .locM .inner").hide()}),c("#loc-38").click(function(){c("#for-bkk").fadeToggle("fast")}),c("#loc-39").click(function(){c("#for-central").fadeToggle("fast")}),c("#loc-40").click(function(){c("#for-east").fadeToggle("fast")}),c("#loc-41").click(function(){c("#for-north-east").fadeToggle("fast")}),c("#loc-42").click(function(){c("#for-south").fadeToggle("fast")}),c("#loc-43").click(function(){c("#for-north").fadeToggle("fast")}),c("#contact_us .loc .inner").click(function(){c(this).removeClass("active"),c(this).toggleClass("active")}),c("#contact_us .loc .place >li").click(function(){c(this).toggleClass("active"),c(this).children(".inner-list").slideToggle("fast")}),c(".portfolio-modal").on("show.bs.modal",function(){c(".lazy_load").each(function(){var o=c(this);o.attr("src",o.data("src"))})})

function checkWidth() {
    var windowsize = window.innerWidth;
	
    if (windowsize < 768) {
        $("li.menu-340 a").attr("href", "https://www.larocheposay-th.com/products/all");
    } else {
        $("li.menu-340 a").attr("href", '#');
    }
}

checkWidth();
})}}}(jQuery);