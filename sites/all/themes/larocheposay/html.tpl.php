<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->  <html xmlns="http://www.w3.org/1999/xhtml" lang="th" xml:lang="th"><!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" /> 
	<meta name="google-site-verification" content="KUS2Q54jT3db69i0tCXFvQjcWJDiTGbcpKYujI6YlBc" />
    <?php if(arg(0) != "products"){?>
		<title><?php print $head_title; ?></title> 
	<?php }?>
    <?php print $head; ?>
    
    

    
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-37194655-2', 'auto');
        //ga('send', 'pageview');
        
        
        function gaClickTracking(pagename) {
			ga('send', 'event', 'shopping', 'clickShopping', pagename);
		}
		
		function gaClickTrackingClick(type, click, pagename) {
			ga('send', 'event', type, click, pagename);
		}

    </script>
  <?php  
/*
<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0068/6587.js";
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>
*/
 ?> 
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TP2G5HF');</script>
<!-- End Google Tag Manager -->  
  </head>
  <body  class="<?php print $classes; ?>" <?php print $attributes; ?>>
    
    <?php
/*
	    <!-- Google Tag Manager -->
	    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WB9WVB"
	    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	    })(window,document,'script','dataLayer','GTM-WB9WVB');</script>
	    <!-- End Google Tag Manager --> 
*/
    ?>
   

    <?php print $page_top; ?>
    <?php print $page; ?>
    <?php print $page_bottom; ?>
    

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TP2G5HF"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
  </body>
  
  <?php print $scripts; ?>
  <?php print $styles; ?>
  
  
  <script type="application/ld+json">
{
  "@context" : "http://schema.org",
  "@type" : "Organization",
  "name" : "larocheposay",
  "url" : "http://www.larocheposay-th.com/",
  "sameAs" : [
    "https://www.facebook.com/LaRochePosayThailand",
    "https://www.youtube.com/channel/UC0_SZhOGJL1pHteexU5eI5Q",
    "http://www.larocheposay-th.com"
 ]
}
</script> 

<!-- Remarketing Code for Keyword Brand / Product -->
<script type="text/javascript">

/* <![CDATA[ */

var google_conversion_id = 968594789;

var google_custom_params = window.google_tag_params;

var google_remarketing_only = true;

/* ]]> */

</script>

<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">

</script>

<noscript>

<div style="display:inline;">

<img height="1" width="1" style="border-style:none;" alt="" 

src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/968594789/?value=0&amp;guid=O

N&amp;script=0"/>

</div>

</noscript>


<!-- Google Code for Remarketing Tag -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 958562044;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/958562044/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>



<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '276938242787313');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=276938242787313&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</html>


